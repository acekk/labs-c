/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal1.internal;


import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;
/**
 *
 * @author st
 */
public class Activator implements BundleActivator{
    
    @Override
    public void start(BundleContext bc) throws Exception {
        bc.registerService(Animal.class.getName(), new Okon(), null);
        Logger.get().log(Animal.class.getName(), "new okon");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        ServiceReference ref = bc.getServiceReference(Animal.class.getName());
        bc.ungetService(ref);
        Logger.get().log(Animal.class.getName(), "rem okon");
    }
}
