/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal2.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;
/**
 *
 * @author st
 */
public class Tygrys implements Animal {
    
    private String status;
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this); 
    
    @Override
    public String getSpecies() {
        return "sTygrys";
    }

    @Override
    public String getName() {
        return "tygrys";
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.pcs.firePropertyChange("status", this.status, status);
        this.status = status;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }
}
